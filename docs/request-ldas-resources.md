# Request a personal VM on LDAS resources
To request a VM to work on your local LLAI development, please open a [Computing Help Desk ticket](https://git.ligo.org/computing/helpdesk/-/issues). 

The ticket should look like:

```
Title: VM provisioning at CIT for local LLAI development

Request of a fluxuserX machine to support personal development of LLAI components.

Specs:
- single node Kubernetes cluster (K3S)
- RL8.9
- 16 vcores
- 32GB RAM
- 64GB disk
- open to anyone with a cluster account
- /scratch/lalsimulation (available)
- /cvmfs/software.igwn.org (available)
- /dev/shm/kafka/ (O3Replay and O4 streams available)
- alias gracedb-devX.ldas.cit
```

You can tag the chairs of the Low Latency working group to ensure they are informed of your request and support it.
