#!/bin/sh

tag=$1
deployment=$2
namespace=$3
repository=$4

# TESTED USING
#  tag="deployment-tools-not-dev-deps"
#  repository="containers.ligo.org/leo-singer/gwcelery"
# AND
#  tag="test"
#  repository="containers.ligo.org/roberto.depietri/gwcelery"

if [ ! $tag ]; then
    echo "USAGE: ./deploy-new-gwcelery-image.sh tag [deployment] [namespace] [repo]"
    echo " tag        : the tag of container"
    echo " deployment : the deployment to updata [gwcelery]"
    echo " namespace  : the k8s namespace  [default]"
    echo " repo       : the image repository [containers.ligo.org/emfollow/gwcelery]"
    exit
fi
if [ ! $deployment ]; then
    deployment="gwcelery"
fi
if [ ! $namespace ]; then
    namespace="default"
fi
if [ ! $repository ]; then
    repository="containers.ligo.org/emfollow/gwcelery"
fi


echo "Deploy as ${deployment} in the namespace ${namespace}"
echo "the following gwcelery image: ${repository}:${tag}."
echo "  (1) Tag       : $tag"
echo "  (2) Deployment: $deployment"
echo "  (3) Namespace : $namespace"
echo "  (4) Repository: $repository"
echo "-----------------------------------------------------------"
echo "Applying command:"
echo "  helm upgrade -n ${namespace} --install  --reuse-values \\"
echo "       --set gwcelery.image.repository=\"${repository}\" \\"
echo "       --set gwcelery.image.tag=\"${tag}\" \\"
echo "       gwcelery gwcelery-helm/gwcelery"
echo "-----------------------------------------------------------"

helm upgrade -n ${namespace} --install  --reuse-values \
     --set gwcelery.image.repository="${repository}" \
     --set gwcelery.image.tag="${tag}" \
     gwcelery gwcelery-helm/gwcelery

