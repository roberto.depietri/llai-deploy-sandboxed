import json
import glob
import os
import numpy as np

from typer import Argument, Exit, Option, Typer, echo

def update_meg_cache_include_localization(SEVENT) :
    GEVENTs=glob.glob(SEVENT+'/G*/')
    print(GEVENTs)
    
    for GEV in GEVENTs:
        with open(GEV+'description.json') as f:
            gev=json.load(f)
    
        if gev['group'] != 'CBC':
            print("SKIP: Event {} is not CBC ({}-{}-{})".format(
                GEV,gev['group'],gev['pipeline'],gev['search']))
            continue
        has_localization = False
        for files in gev['uploads']:
            if 'sky_loc' in files['tags']:
                has_localization = True
        if has_localization :
            print("SKIP: Event {} has localization and is CBC ({}-{}-{})".format(
                GEV,gev['group'],gev['pipeline'],gev['search']))
            continue
        
        delays=np.array([files['delay'] for files in gev['uploads']])
        print("Event {} is CBC ({}-{}-{})".format(
            GEV,gev['group'],gev['pipeline'],gev['search']))

            
        locfile = glob.glob(GEV+'*multiorder*.fits*')[0]
        locfilename=os.path.split(locfile)[1]
        if 'fits' in locfilename:
            print('  LOCALIZATION -----',locfile,locfilename)
            extra_entry = [{'message': 'Offline LOCALIZATION', 'tags': ['sky_loc'], 'filename': locfilename, 'delay': 2.0}]
        else:
            extra_entry = []

        print('--',np.array(delays)[:-1])
        filenames=[files['filename'] for files in gev['uploads']]
        before_2s = np.where(delays<=2.0)[0]
        after_2s  = np.where((delays>2.0)*(delays<=120.0))[0]
        new_uploads = ( list(np.array(gev['uploads'])[before_2s]) +
                        extra_entry +
                        list(np.array(gev['uploads'])[after_2s]))
        new_gev = gev.copy()
        new_gev['uploads'] = new_uploads
    
        for files in gev['uploads']:
            print('  <--',files['delay'],files['filename'])
            print('  -->',files['delay'],files['filename'])
    
        for new_entry in new_uploads:
            # print(new_entry)
            pass
        with open(GEV+'description.json','tw') as f:
            json.dump(new_gev,f,indent=4)
        with open(GEV+'description_old.json','tw') as f:
            json.dump(gev,f,indent=4)

SEVENT='/Users/roberto.depietri/VIRGO/EMfollow/O4_ALERT_Results/production/S230831e'
print(SEVENT)
update_meg_cache_include_localization(SEVENT)


