#!/bin/sh

app=$1
command=$2
namespace=$3

if [ ! $app ]; then
    echo "USAGE: ./command-pod app command [NAMESPACE]"
    echo ""
    echo "Examples:"
    echo "     ./command-pod.sh meg \"ls -l\""
    exit
fi

if [ ! $namespece ]; then
    namespace="default"
fi

echo "Entering app: $app"

pod=`kubectl get pods -n $namespace | grep $app | head -n 1 | awk '{print $1}'`
[ -z "$pod" ] && echo "ERR: no such pod" && exit 1

COMMAND="kubectl exec -n ${namespace} -it ${pod}  -- bash -c \" ${command} \""
echo "===================================================="
echo EXECUTING: $COMMAND 
echo "===================================================="

# Now execute the command!
kubectl exec -n ${namespace} -it ${pod}  -- bash -c " ${command} "

