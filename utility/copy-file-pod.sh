#!/bin/sh

app=$1
file=$2
tofile=$3
namespace=$4

if [ ! $app ]; then
    echo "USAGE: ./copy-file-pod.sh pod file tofile [NAMESPACE]"
    echo ""
    echo "Examples:"
    echo ""
    exit
fi

if [ ! $namespece ]; then
    namespace="default"
fi

pod=`kubectl get pods -n $namespace | grep $app | head -n 1 | awk '{print $1}'`
[ -z "$pod" ] && echo "ERR: no such pod" && exit 1


COMMAND="kubectl cp ${file} ${namespace}/${pod}:${tofile}"
echo "===================================================="
echo EXECUTING: $COMMAND
echo "===================================================="

# Now execute the command!
$COMMAND


