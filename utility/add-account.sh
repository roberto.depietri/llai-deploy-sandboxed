#!/bin/bash


#namespace="default"
#release="gracedb"

user=$1
deployment=$2
namespace=$3

if [ ! $user ]; then
    echo ""
    echo "USAGE: ./add-account.sh user [deployment] [namespace]"
    echo "    user       : the LIGO username to add (e.g. albert.einstein) "
    echo "    deployment : the gracedb deployment [gracedb]"
    echo "    namespace  : the k8s namespace  [default]"
    echo "-----------------------------------------------------------------"
    echo "N.B.(1): if the environment variable ALL_PERMS is defined then"
    echo "         the albert.einstien USER will be granted all permisions."
    echo "N.B.(2): if the environment variable LDAP_CREDS is defined then"
    echo "         username and password will be derived by the variable."
    echo "N.B.(3): if the environment variable LIGO_USERS is defined then"
    echo "         all the LIGO user will be added."
    echo "-----------------------------------------------------------------"
    exit
fi

if [ ! $deployment ]; then
    deployment="gracedb"
fi
if [ ! $namespace ]; then
    namespace="default"
fi


echo "*** Configuring keytab values"
if [[ -z $LDAP_CREDS ]]; then
  ## "Insert your LIGO username (i.e. albert.einstein)":
  ##  user
  username="$user@LIGO.ORG"
  echo "Insert your LIGO [${username}] password":
  read -s password
  echo "Insert gracedb password for [${username}]":
  read -s gracedb_password
else
  username="$(cut -d":" -f1 <<< $LDAP_CREDS)@LIGO.ORG"
  password="$(cut -d":" -f2 <<< $LDAP_CREDS)"
fi

# Configure keytab to be used inside the pod
KEYTAB_FILE=/tmp/krb5cc_0
KEYTAB_SECURITY="aes128-sha1"

# Get the pod name
pod=`kubectl get pods -n $namespace | grep ${deployment}-0 | head -n 1 | awk {'print $1}'`
echo "Commands will be executed in the ${pod} pod"


# Remove keytab file if it is already presen inside the pod
kubectl exec -n $namespace -c ${deployment} $pod -i -- rm $KEYTAB_FILE 

# Create or edit keytab file inside the pod
echo "*** Creating keytab ***" 
kubectl exec -n $namespace -c ${deployment} $pod -i -- ktutil <<EOF
add_entry -password -p $username -k 0 -e $KEYTAB_SECURITY
$password
write_kt $KEYTAB_FILE
quit
EOF

# Init Kerberos
echo "*** Init Kerberos"
kubectl exec -n $namespace -c ${deployment} $pod -- kinit -k -t $KEYTAB_FILE $username

# ---------IF ONE NEED TO COPY THE COMMAND TO THE GRACEDB POD -----
# COMMAND="get_or_edit_account.py"
# GRACEDB_DIR="/app/gracedb_project/gracedb"
# COMMAND_DIR="ligoauth/management/commands"
# kubectl cp ${COMMAND} default/${pod}:${GRACEDB_DIR}/${COMMAND_DIR}/${COMMAND}
# -------------------------------------------

# Add superuser from ligo ldap
echo "*** Add LDAP user"
kubectl exec -n $namespace -c ${deployment} $pod -- \
  python3 manage.py get_or_edit_account $username -p "$gracedb_password" -s

if [ ! -z "${ALL_PERMS}" ]; then
    echo "*** Add ALL PERMISION ***"
    kubectl exec -n $namespace -c ${deployment} $pod -- \
        python3 manage.py shell -c \
      "\
from django.contrib.auth.models import Permission
from gracedb.core.models import UserModel
user = UserModel.objects.get(username=\"$username\".lower())
for perm in Permission.objects.all():
  user.user_permissions.add(perm)
      "
fi

if [ ! -z "${LIGO_USERS}" ]; then
    echo "*** Adding all LIGO users ***"
    kubectl exec -n $namespace -c ${deployment} $pod -- \
        python3 manage.py update_user_accounts_from_ligo_ldap ligo
fi
