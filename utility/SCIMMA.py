#!/usr/bin/env python


import logging
import shutil
import sys
import os
import re
from io import BytesIO
import json

from pathlib import Path
from typing import Optional
from typer import Argument, Exit, Option, Typer, echo


from datetime import datetime
from astropy.time import Time
from astropy.table import Table
import astropy_healpix as ah


import hop
from hop import Stream, auth
from hop.auth import Auth
from apischema import serialize

import numpy as np

SCIMMA = Typer(help='SCIMMA utility')

@SCIMMA.command()
def receiver(
    server: str = Option(
        'hopskotch',
        help='kafka server name',
    ),
    topic: str = Option(
        "igwn.gwalert-minikube",
        help = 'topic used for gwalerts',
    ),
    auth: bool = Option(
        False,
        help='use SCIMMA authentication'
    ),
    dump: bool = Option(
        True,
        help='Dump to disk received messages'
    )
) -> None:
    """Receive gwalert fron a SCIMMA server."""
    KAFKA_SERVER='kafka://' + server  + '/' + topic
    print(f'SCIMMA receiver start for {KAFKA_SERVER}')
    ##auth = Auth("my-username", "my-password")
    if not auth :
        stream = Stream(False)
    else:
        stream = Stream()
    
    with stream.open(KAFKA_SERVER, 'r') as s:
        for message in s:
            messagegroup = type(message)
            messagetype = type(message.content)
            print(f'--- Message received --- '
                  f'CLASS: {messagegroup} TYPE: {messagetype}')
            if messagetype == str :
                # igwn-alert send str type
                json_message = json.loads(message.content)
                parse_notice(json_message,dump)
                # print(json_message)
                #print(type(message))
            elif messagetype == list :
                # igwn-alert scimma kafka alert are list
                parse_notice(message.content[0],dump)

@SCIMMA.command()
def topics(
    server: str = Option(
        'hopskotch',
        help='kafka server name',
    ),
    filter: str = Option(
        "",
        help = 'regex filter of the available topics',
    ),
    auth: bool = Option(
        False,
        help='use SCIMMA authentication'
    )
) -> None:
    """List available topics on the SCIMMA server."""
    KAFKA_SERVER='kafka://' + server
    print(f'SCIMMA available topics on the server {KAFKA_SERVER}')
    ##auth = Auth("my-username", "my-password")
    ## Neet to write the command that list the avilable topics
    ## https://hop-client.readthedocs.io/en/latest/
    if not auth :
        available_topics= list(hop.io.list_topics(KAFKA_SERVER,False))
    else:
        available_topics= list(hop.io.list_topics(KAFKA_SERVER))

    try:
        re_filter = re.compile('[\S]*'+filter+'[\S]*')
        for topic in available_topics:
            if re_filter.match(topic):
                print(f'--- matched - {topic}')
    except:
        print('Error : [\S]*'+filter+'[\S]* is not a valid filter')

@SCIMMA.command()
def info() -> None:
    print("""
    #KAFKA_SERVER='kafka://kafka.scimma.org/igwn.gwalert-playground'
    #KAFKA_SERVER='kafka://kafka.scimma.org/igwn.gwalert-minikube'
    """)

def parse_notice(record,dump: bool=True):
    # Only respond to mock events. Real events have GraceDB IDs like
    # S1234567, mock events have GraceDB IDs like M1234567.
    # NOTE NOTE NOTE replace the conditional below with this commented out
    # conditional to only parse real events.
    # if record['superevent_id'][0] != 'S':
    #    return

    time_now=datetime.utcnow()
    try:
        time_record=(record['event']['time']).split('.')[0]
        # ValueError: time data '2023-03-22T14:16:47' does not match format '%Y-%m-%d %H:%M:%S'
        event_time= datetime.strptime(time_record, '%Y-%m-%dT%H:%M:%S')
        delta_time= (time_now-event_time).seconds
    except:
        delta_time = 0.0

    alert_type= record.get('alert_type','--not-present--')
    sevent= record.get('superevent_id','KAFKA_MESSAGE')

    # -----------------------------------------------
    # if dump == True ==> SAVE TO DISK THE MESSAGE
    # -----------------------------------------------
    if dump:
        Path(sevent).mkdir(parents=True, exist_ok=True)
        FNAME="{}/{}_message_{}_{}.json".format(sevent,sevent,alert_type,time_now)
        # Print remaining fields
        print(f'  - ALERT_TYPE={alert_type} latency: {delta_time} s')
        print(f'           - Record: {FNAME}')
        for field in list(record):
            value_field = str(record[field])
            print(f'  -        {field} : {value_field[:90]}')
        path= Path(FNAME.replace(' ','_'))
        description= { 'superevent_id' : sevent,
                       'alert_type': alert_type,
                       'latency': delta_time,
                       'record': record}
        path.write_text(json.dumps(serialize(description), indent=4))
                    

    # Parse sky map (If present)
    try:
        event_field = record.get('event',None)
        skymap_bytes = event_field.get('skymap',None)
        if skymap_bytes:
            # Parse skymap directly and print most probable sky location
            skymap = Table.read(BytesIO(skymap_bytes))
            level, ipix = ah.uniq_to_level_ipix(
                skymap[np.argmax(skymap['PROBDENSITY'])]['UNIQ']
            )
            ra, dec = ah.healpix_to_lonlat(ipix, ah.level_to_nside(level),
                                           order='nested')
            print(f'  - Most probable sky location (RA, Dec) = ({ra.deg}, {dec.deg})')
            # Print some information from FITS header (if they are present)
            try:
                distmean =skymap.meta["DISTMEAN"]
                diststd  =skymap.meta["DISTSTD"]
                print(f'  - Distance = {distmean} +/- {diststd}')
            except:
                distmean = 0.0
                diststd  = 0.0
    except:
        pass

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(SCIMMA())
